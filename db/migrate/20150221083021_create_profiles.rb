class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :username
      t.string :mobile
      t.date :dob
      t.string :gender
      t.string :address
      t.string :city
      t.string :education
      t.string :skill
      t.string :company_name
      t.string :experience
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :profiles, :users
  end
end
