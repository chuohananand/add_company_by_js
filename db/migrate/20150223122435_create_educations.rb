class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.string :name
      t.references :profile, index: true

      t.timestamps null: false
    end
    add_foreign_key :educations, :profiles
  end
end
