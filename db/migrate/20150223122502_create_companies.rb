class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.references :profile, index: true

      t.timestamps null: false
    end
    add_foreign_key :companies, :profiles
  end
end
