class InvitesController < ApplicationController
   before_action :status
  def new
  	@invite = Invite.new
  end

  def create
     if @user = User.find_by_email(params[:invite][:email])
    	   redirect_to new_invite_path ,	notice: 'This Email already Registerd.'
       else
         @invite = current_user.invites.build(invite_params)
	  	   @invite.save
	  	   UserMailer.invite_email(@invite).deliver_later
	  	  redirect_to root_path , notice: 'Invitation  successfully Sended.'
      end
  end

  private

  def invite_params
      params.require(:invite).permit(:email, :user_id)
    end
  def status
    (redirect_to root_path, alert: 'unauthorised access') unless current_user.is_admin.present?
  end
end
