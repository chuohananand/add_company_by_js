class ProfilesController < ApplicationController

	
  def search
    @q = Profile.ransack(params[:q])
    @profiles = @q.result
  end

  def show
    @profile = Profile.find(params[:id])
    @skill = @profile.skills

  end

	def new
		@profile = Profile.new
    @skill = Skill.new
    @education = Education.new
    @company = Company.new
  end

  def edit
    @profile = current_user.profile
    @skill = @profile.skills
    @company = @profile.companys
    @education = @profile.educations
  end
 
  def create
    @profile = current_user.build_profile(profile_params)
   if @profile.save
      redirect_to root_path,  notice: 'Profile Successfully create .'
     else
       render 'new' 
    end
  end

  def update
       @profile = Profile.find_by_user_id(params[:id])
   if  @profile.update(profile_params)
       redirect_to root_path
      else
       render 'edit'
    end
 end

  private

  def profile_params
    params.require(:profile).permit(:username, :mobile, :dob, :gender, :address, :city, :education, :skill, :company_name, :experience, :user_id, :image, skills_attributes: [:id, :name], companys_attributes: [:id, :name], educations_attributes: [:id, :name])
  end
end
