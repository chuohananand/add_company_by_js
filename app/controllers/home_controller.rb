class HomeController < ApplicationController

  def index
  end
  
  def show_profile
  	@profile = Profile.new
  end

  def user_profile
  	@user = User.find(params[:user_id])
  	@profile = @user.profile
  end
end
