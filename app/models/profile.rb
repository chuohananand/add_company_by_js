class Profile < ActiveRecord::Base
  has_many :skills
  has_many :educations
  has_many :companys
  belongs_to :user
	accepts_nested_attributes_for :skills, :reject_if => :all_blank, :allow_destroy => true
	accepts_nested_attributes_for :educations, :reject_if => :all_blank, :allow_destroy => true
	accepts_nested_attributes_for :companys, :reject_if => :all_blank, :allow_destroy => true
  
  # has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
  # validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  

has_attached_file :image
validates_attachment :image, :content_type => {:content_type => %w(image/jpeg image/jpg image/png application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document)}
end
